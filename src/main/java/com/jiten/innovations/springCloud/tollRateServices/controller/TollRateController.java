package com.jiten.innovations.springCloud.tollRateServices.controller;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jiten.innovations.springCloud.tollRateServices.pojo.MessageInfo;
import com.jiten.innovations.springCloud.tollRateServices.pojo.TollRate;

@RestController
public class TollRateController {
	
	private static final Logger LOG = Logger.getLogger(TollRateController.class.getName());
	
	@Autowired
	Environment environment;
	
	@RequestMapping("/tollRate/{stationId}")
	public TollRate getTollRate(  @RequestHeader("AUTH_HEADER") String AUTH_HEADER,   @PathVariable int stationId) {
		
		TollRate tr;
		int portNo = Integer.valueOf(environment.getProperty("local.server.port"));
		String property = AUTH_HEADER;
		System.out.println("AUTH_HEADER :: " + property);
		switch(stationId) {
		case 1:
			tr = new TollRate(stationId, new BigDecimal(0.55), Instant.now().toString(),new MessageInfo(portNo));
		break;	
		
		case 2:
			tr = new TollRate(stationId, new BigDecimal(0.75), Instant.now().toString(),new MessageInfo(portNo));
		break;
		
		case 3:
			tr = new TollRate(stationId, new BigDecimal(0.90), Instant.now().toString(),new MessageInfo(portNo));
		break;
		
		default:
			tr = new TollRate(stationId, new BigDecimal(1.00), Instant.now().toString(),new MessageInfo(portNo));
		break;
		
		}
		
		return tr;
	}
	
	@RequestMapping("/tollRate/fixed")
	public String getTollRateFixed() {
		LOG.info("inside getTollRateFixed ------------ ");
		return "20";
	}
	
}
