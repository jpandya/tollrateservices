package com.jiten.innovations.springCloud.tollRateServices.healthCheck;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class TollRateServiceHealthCheck implements HealthIndicator{

	private int errorCode=0;
	
	@Autowired
	Environment environment;
	
	@Override
	public Health health() {

		String port = environment.getProperty("local.server.port");
		System.out.println("port = " + port);
		String url = environment.getProperty("local.server.host");
		try {
			System.out.println("url = " + InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		System.out.println("TollRateServiceHealthCheck :: Health Indicator :: ERROR CODE="+errorCode);
		if(port!= null && port.equalsIgnoreCase("8086")) {
			if(errorCode>2 && errorCode < 10) {
				errorCode++;
				return Health.down().withDetail("Custom Error Code", errorCode).build();
			}
		}
		
		errorCode++;
		return Health.up().build();
		
		
	}

}
