package com.jiten.innovations.springCloud.tollRateServices.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.jiten.innovations.springCloud.tollRateServices.controller"
																				  ,
																				  "com.jiten.innovations.springCloud.tollRateServices.healthCheck"
																				 })
@EnableEurekaClient
@SpringBootApplication
public class TollRateServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TollRateServicesApplication.class, args);
	}

}
