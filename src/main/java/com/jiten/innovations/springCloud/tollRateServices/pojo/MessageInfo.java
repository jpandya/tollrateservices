package com.jiten.innovations.springCloud.tollRateServices.pojo;

public class MessageInfo {
	private int port;

	
	
	public MessageInfo(int port) {
		super();
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	
}
