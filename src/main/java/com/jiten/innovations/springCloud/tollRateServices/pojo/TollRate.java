package com.jiten.innovations.springCloud.tollRateServices.pojo;

import java.math.BigDecimal;

public class TollRate {
	private int stationId;
	private BigDecimal currentRate;
	private String timestamp;
	private MessageInfo messageInfo;
	public TollRate(int stationId, BigDecimal currentRate, String timestamp, MessageInfo messageInfo) {
		super();
		this.stationId = stationId;
		this.currentRate = currentRate;
		this.timestamp = timestamp;
		this.messageInfo = messageInfo;
	}
	public int getStationId() {
		return stationId;
	}
	public void setStationId(int stationId) {
		this.stationId = stationId;
	}
	public BigDecimal getCurrentRate() {
		return currentRate;
	}
	public void setCurrentRate(BigDecimal currentRate) {
		this.currentRate = currentRate;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public MessageInfo getMessageInfo() {
		return messageInfo;
	}
	public void setMessageInfo(MessageInfo messageInfo) {
		this.messageInfo = messageInfo;
	}
	
	
	
	
}
